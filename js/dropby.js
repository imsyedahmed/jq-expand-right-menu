(function($){
 $.fn.dropby = function(options) {

  var defaults = {
    aclass : 'active',
    speed  : 500
  };

  var options = $.extend(defaults, options);
    
  return this.each(function() {
    /// here goes to all function 
    var obj = $(this);
    /// findout which li element contains sub menu 
    var subul = obj.children().find("ul");
    var submenus = subul.parent("li");
    /// hide subul's all li 
    subul.find("li").hide();

    /// submenu li element hover style
    submenus.hover(function(){
        /// add class active
        $(this).addClass(options.aclass);
        /// now show one by one sub menu element 
        var nestedchild = $(this).children('ul').find('li');
        nestedchild.each(function(index){
          var li = $(this);
              setTimeout(function() {
                    li.slideDown(options.speed);
              }, options.speed * index);
        });

    },
    function(){
      /// on hover out remove class active 
      $(this).removeClass(options.aclass);
      var nestedchild = $(this).children('ul').find('li');
      nestedchild.slideUp('fast');
      nestedchild.hide();
    }
    );

  });
 };
})(jQuery);
